import java.io.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement; 
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.mysql.jdbc.*;

import java.awt.*;

public class Main
{
	public static String user = "root";
	public static String pw = "sammy2000";
	public static String sql;
	public static Connection c = null;
	public static Statement stmt = null;
	public static String pfad="";
	public static String dateiname=null;
	public static ArrayList<String> in = new ArrayList<String>();
	public static double[][] tempData= {{7.4,-12.6},{18.8,-4.3},{22.5,0,8},{24.6,0.9},{32.4,0.8},{34.6,11.5},{34.6,11.9},{38.4,10.8},{27.3,7.2},{23.2,4.8},{16.6,-1.0},{14.8,-4.8}};
	public static double lastTemp=99;
	public static DecimalFormat df=new DecimalFormat(".#");
	public static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

	public static void main( String args[] ){
		try {
			prepareDatabase();

			char choice;
			do {
				choice=insertionMenu();
				switch(choice) {
				case 'i':
					System.out.println("Wie warm ist es gerade?");
					insertTemp(Double.parseDouble(console.readLine().replaceAll(",", ".")));
					break;
				case 'z':
					insertTemp(getDateTime(), Double.parseDouble(console.readLine().replaceAll(",", ".")));
					break;
				case 'r': randomTempMeasure();
				case 'd': datenAnzeigen();
				break;
				}
			}while(true);


		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public static void datenAnzeigen() throws IOException, SQLException {
		File file = new File("./data.txt");
		file.delete();
		file= new File("./dateData.txt");
		file.delete();
		
		FileWriter fw = new FileWriter("data.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		FileWriter fw2 = new FileWriter("dateData.txt");
		BufferedWriter bw2 = new BufferedWriter(fw2);

		sql="SELECT * FROM temperatur ORDER BY zeit;";

		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		bw.write("[\""+rs.getString("temperatur")+"\"");
		bw2.write("[\""+rs.getString("zeit")+"\"");
		while (rs.next()) {
			bw.write(",\""+rs.getString("temperatur")+"\"");
			bw2.write(",\""+rs.getString("zeit")+"\"");
		}
		bw.write("]");
		bw2.write("]");
		rs.close();
		stmt.close();

		bw.close();
		fw.close();
		bw2.close();
		fw2.close();
		
		file=new File("./Chart.html");
		Desktop desk = Desktop.getDesktop();
		desk.open(file);
	}

	public static char insertionMenu() throws IOException {
		System.out.println("d ... Daten anzeigen");
		System.out.println("i ... aktuelle Temperatur eintragen");
		System.out.println("z ... Temperatur zu bestimmten Zeitpunkt eintragen");
		System.out.println("r ... alle 5 Minuten eine zufällige Temperatur eintragen (zum Abbruch ist ein Neustart");
		System.out.println("Ihre Wahl: ");
		char c = console.readLine().charAt(0);
		return c;
	}

	public static Calendar getDateTime() throws NumberFormatException, IOException {
		Calendar datetime = Calendar.getInstance();
		System.out.println("In welchem Jahr wurde die Temperaturmessung vollzogen?");
		int year=Integer.parseInt(console.readLine());
		System.out.println("In welchem Monat wurde die Temperaturmessung vollzogen?");
		int month=Integer.parseInt(console.readLine());
		System.out.println("An welchem Tag wurde die Temperaturmessung vollzogen?");
		int day=Integer.parseInt(console.readLine());
		System.out.println("In welcher Stunde wurde die Temperaturmessung vollzogen?");
		int hour=Integer.parseInt(console.readLine());
		System.out.println("In welcher Minute wurde die Temperaturmessung vollzogen?");
		int minute=Integer.parseInt(console.readLine());

		datetime.set(year, month-1, day, hour, minute, 0);

		System.out.println("Wie warm war es zum gewünschten Zeitpunkt?");
		return datetime;
	}

	public static void prepareDatabase() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		/*
		System.out.println("Bitte MySQL-User angeben: ");
		user = console.readLine();

		System.out.println("Bitte MySQL-Passwort eingeben: ");
		pw = console.readLine();
		 */
		c = DriverManager.getConnection("jdbc:mysql://localhost:3306", user, pw);

		stmt = c.createStatement();
		sql="CREATE DATABASE IF NOT EXISTS fakeTemp;";
		stmt.executeUpdate(sql);
		stmt.close();

		stmt = c.createStatement();
		sql="USE fakeTemp;";
		stmt.executeUpdate(sql);
		System.out.println("Database in use");
		stmt.close();

		System.out.println("");

		stmt = c.createStatement();
		sql="CREATE TABLE IF NOT EXISTS temperatur(id INT, temperatur REAL, zeit DATETIME);";
		stmt.executeUpdate(sql);
		stmt.close();
	}

	public static void randomTempMeasure() throws SQLException, InterruptedException {
		while(true) {
			insertTemp(getRandomTemp());
			TimeUnit.MINUTES.sleep(5);
		}
	}

	public static double getRandomTempOfLastEntry() throws SQLException {
		ResultSet rs;
		sql = "SELECT * FROM temperatur WHERE id="+lastID()+";";
		stmt = c.createStatement();
		rs = stmt.executeQuery(sql);
		rs.next();
		double lastMeasure=Double.parseDouble(rs.getString("temperatur"));
		rs.close();
		stmt.close();

		double change= Math.random()*2-1;

		return lastMeasure+change;
	}

	public static double getRandomTemp() throws SQLException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String datetime=dateFormat.format(cal.getTime());
		int time=Integer.parseInt(datetime.substring(11, 13));
		int month=Integer.parseInt(datetime.substring(5, 7))-1;

		if(lastTemp==99) {
			if((time<6)||(time>22))
				lastTemp=Math.random()*(tempData[month][0]/1.5-tempData[month][1])+tempData[month][1];
			else
				lastTemp=Math.random()*(tempData[month][0]-tempData[month][1])+tempData[month][1];
			return lastTemp;
		}

		double change;
		if((time<6)||(time>20))
			change= Math.random()*(-1);
		else
			change= Math.random()*2-1;

		lastTemp=lastTemp+change;

		return lastTemp;
	}

	public static int lastID() throws SQLException {
		ResultSet rs;
		stmt = c.createStatement();
		sql = "SELECT * FROM temperatur ORDER BY id DESC LIMIT 1;";
		rs = stmt.executeQuery(sql);
		rs.next();
		int id = Integer.parseInt(rs.getString("id"));
		rs.close();
		stmt.close();
		return id;
	}

	public static void insertTemp(double temperatur) throws SQLException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();

		PreparedStatement ps = c
				.prepareStatement("INSERT INTO temperatur VALUES(?,?,?);");
		ps.setInt(1, (lastID()+1));
		ps.setDouble(2, Double.parseDouble(df.format(temperatur).replaceAll(",", "."))); 
		ps.setString(3, dateFormat.format(cal.getTime()));

		ps.executeUpdate();
		ps.close();
	}

	public static void insertTemp(Calendar zeit, double temperatur) throws SQLException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		PreparedStatement ps = c
				.prepareStatement("INSERT INTO temperatur VALUES(?,?,?);");
		ps.setInt(1, (lastID()+1));
		ps.setDouble(2, Double.parseDouble(df.format(temperatur).replaceAll(",", "."))); 
		ps.setString(3, dateFormat.format(zeit.getTime()));

		ps.executeUpdate();
		ps.close();
	}
}
