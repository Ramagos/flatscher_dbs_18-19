import java.io.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement; 
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.mysql.jdbc.*;

import java.awt.*;

public class SQLInjection
{
	public static String user = "root";
	public static String pw = "sammy2000";
	public static String sql;
	public static Connection c = null;
	public static Statement stmt = null;
	public static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

	public static void main( String args[] ){
		try {
			prepareDatabase();

			//   X" OR "1"="1

			System.out.println("Was ist ihr IBAN?");
			String sel=console.readLine();
			System.out.println("Ausgabe ohne preparedStatement:");
			select(sel);
			System.out.println("");
			System.out.println("Ausgabe mit preparedStatement:");
			selectPrepared(sel);
			System.err.println("\nEnde des Programmes");
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public static void prepareDatabase() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		/*
		System.out.println("Bitte MySQL-User angeben: ");
		user = console.readLine();

		System.out.println("Bitte MySQL-Passwort eingeben: ");
		pw = console.readLine();
		 */
		c = DriverManager.getConnection("jdbc:mysql://localhost:3306", user, pw);

		stmt = c.createStatement();
		sql="CREATE DATABASE IF NOT EXISTS testInjection;";
		stmt.executeUpdate(sql);
		stmt.close();

		stmt = c.createStatement();
		sql="USE testInjection;";
		stmt.executeUpdate(sql);
		System.out.println("Database in use");
		stmt.close();

		System.out.println("");
	}

	public static void insert(String name, int alter) throws SQLException {
		sql = "INSERT INTO student VALUES(\""+name+"\", "+alter+");";
		stmt = c.createStatement();
		stmt.executeUpdate(sql);
		stmt.close();
	}

	public static void select(String iban) throws SQLException {
		sql="SELECT * FROM bankdaten WHERE iban=\""+iban+"\"";

		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		while (rs.next()) {
			System.out.println(rs.getString("iban")+"  -  "+rs.getString("pw"));
		}
		rs.close();
	}
	
	public static void selectPrepared(String iban) throws SQLException {
		sql="SELECT * FROM bankdaten WHERE iban=?";
		PreparedStatement ps = c.prepareStatement(sql);
		
		ps.setString(1, iban);
		
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			System.out.println(rs.getString("iban")+"  -  "+rs.getString("pw"));
		}

		rs.close();
	}
}