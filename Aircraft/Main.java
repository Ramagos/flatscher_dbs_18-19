import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.*;
import java.sql.*;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;

public class Main
{
	public static String user = "root";
	public static String pw = "sammy2000";
	public static String sql;
	public static Connection c = null;
	public static Statement stmt = null;
	public static String pfad="";
	public static String dateiname=null;
	public static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

	public static void main( String args[] ){
		try {
			prepareDatabase();

			char choice;
			do {
				choice=insertionMenu();
				switch(choice) {
				case 'd':
					datenMonatAnzeigen();
					break;

				case 'k':
					karteAnzeigen();
					break;
				}
			}while(true);


		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public static void karteAnzeigen() throws IOException, SQLException {
		BufferedImage img = ImageIO.read(new File("./karte.png"));
		Graphics2D g2d=img.createGraphics();
		g2d.setColor(new Color(255,0,0,4));

		sql="SELECT * FROM DUMP1090DATA ORDER BY seentime;";

		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		while (rs.next()) {
			double xD=rs.getDouble("lon");
			double yD=rs.getDouble("lat");
			//9.818479 13.27 			3,451521 1264
			//47.806660 46.604454		1,202206 656

			int x = (int) (366.215358388*(xD-9.818479));
			int y = (int) (545.663555164*(yD-46.604454));
			if(x<2)
				x=2;
			if(y<2)
				y=2;
			if(x>654)
				x=654;
			if(y>1262)
				y=1262;

			g2d.fillOval(x-2, y-2, 4, 4);
		}
		rs.close();
		stmt.close();

		g2d.dispose();

		ImageIO.write(img, "png", new File("./karte_bearbeitet.png"));
		File file=new File("./karte_bearbeitet.png");
		Desktop desk = Desktop.getDesktop();
		desk.open(file);
	}

	public static void datenMonatAnzeigen() throws IOException, SQLException {
		File file = new File("./data.txt");
		file.delete();

		FileWriter fw = new FileWriter("data.txt");
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write("["+getAircraftsFromNow(12)+"");
		for(int i=11;i>=1;i--) {
			bw.write(","+getAircraftsFromNow(i));
		}
		bw.write("]");

		bw.close();
		fw.close();

		file=new File("./Chart.html");
		Desktop desk = Desktop.getDesktop();
		desk.open(file);
	}
	public static String getAircraftsFromMax(int month) throws IOException, SQLException {
		sql="SELECT MAX(seentime) FROM DUMP1090DATA;";
		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		long max = Long.parseLong(rs.getString("MAX(seentime)"));

		Date date=new Date(max*1000);

		GregorianCalendar untergrenze=new GregorianCalendar();
		untergrenze.setTime(date);
		untergrenze.add(GregorianCalendar.MONTH, -month);

		GregorianCalendar obergrenze=new GregorianCalendar();
		obergrenze.setTime(date);
		obergrenze.add(GregorianCalendar.MONTH, -month+1);

		sql="SELECT COUNT(seentime) FROM DUMP1090DATA WHERE "
				+ "seentime>"+untergrenze.getTimeInMillis()/1000+" AND "
				+ "seentime<"+obergrenze.getTimeInMillis()/1000+";";

		stmt = c.createStatement();
		rs = stmt.executeQuery(sql);
		rs.next();

		String count = rs.getString("COUNT(seentime)");

		rs.close();
		stmt.close();

		return count;
	}
	
	public static String getAircraftsFromNow(int month) throws IOException, SQLException {
		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		GregorianCalendar untergrenze=new GregorianCalendar();
		untergrenze.add(GregorianCalendar.MONTH, -month);

		GregorianCalendar obergrenze=new GregorianCalendar();
		obergrenze.add(GregorianCalendar.MONTH, -month+1);

		sql="SELECT COUNT(DISTINCT seentime) FROM DUMP1090DATA WHERE "
				+ "seentime>"+untergrenze.getTimeInMillis()/1000+" AND "
				+ "seentime<"+obergrenze.getTimeInMillis()/1000+";";

		stmt = c.createStatement();
		rs = stmt.executeQuery(sql);
		rs.next();

		String count = rs.getString("COUNT(DISTINCT seentime)");

		rs.close();
		stmt.close();

		return count;
	}

	public static char insertionMenu() throws IOException {
		System.out.println("d ... Anzahl der Flugzeuge in den letzten 12 Monaten anzeigen");
		System.out.println("k ... Karte");
		System.out.println("Ihre Wahl: ");
		char c = console.readLine().charAt(0);
		return c;
	}
	

	public static void prepareDatabase() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		/*
		System.out.println("Bitte MySQL-User angeben: ");
		user = console.readLine();

		System.out.println("Bitte MySQL-Passwort eingeben: ");
		pw = console.readLine();
		 */
		c = DriverManager.getConnection("jdbc:mysql://localhost:3306", user, pw);

		stmt = c.createStatement();
		sql="USE aircraft;";
		stmt.executeUpdate(sql);
		System.out.println("Database in use");
		stmt.close();

		System.out.println("");
	}
}
